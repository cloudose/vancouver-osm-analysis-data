---
layout: page
title: "TransLink area: Bus stops mapping issues"
permalink: buses/incorrect.html
categories: bus_stops
---
OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).

## Duplicate bus stops numbers

## Incorrectly mapped bus stop numbers (0)
These bus stops have an identifiable 5-digit bus stop number. However, the number is mapped in `name` or `name:en` but not `ref`.
